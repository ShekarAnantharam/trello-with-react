import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Boards from "./Components/Boards";
import Board from "./Components/Board";

class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Boards} />
          <Route path="/board/:id" component={Board} />
        </Switch>
      </Router>
    );
  }
}

export default App;
