import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

import * as API from "./API/api";
import "./styles.css";

class Boards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boards: [],
      createBoard: false,
      boardName: ""
    };
  }

  handleInput = name => {
    this.setState({ boardName: name });
  };
  handleCreateBoard = () => {
    this.setState({ createBoard: true });
  };
  handleCloseCreateBoard = () => {
    this.setState({ createBoard: false });
  };
  handleCreateNewBoard = async () => {
    let newBoard=await API.createBoard(this.state.boardName);
    this.setState({boards:[...this.state.boards, newBoard]})
  };
  async getBoards() {
    let data = await API.getBoards();

    this.setState({ boards: data });
  }
  componentDidMount() {
    this.getBoards();
  }
  render() {
    // console.log(this.state.boards);
    return (
      <Fragment>
        <div className="container">
          <div onClick={this.handleCreateBoard} className="create-board">
            create new board
          </div>
          {this.state.createBoard ? (
            <div>
              <input
                type="text"
                onChange={event => this.handleInput(event.target.value)}
              />
              <p>
                <button onClick={this.handleCreateNewBoard}>
                  create board
                </button>
                <button onClick={this.handleCloseCreateBoard}>X</button>
              </p>
            </div>
          ) : null}
          {this.state.boards.map(board => {
            return (
              <div key={board.id} className="board bg-primary">
                <Link to={`/board/${board.id}`}>{board.name}</Link>
              </div>
            );
          })}
        </div>
      </Fragment>
    );
  }
}

export default Boards;
