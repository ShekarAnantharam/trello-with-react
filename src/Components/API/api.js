import axios from "axios";

let key = "46ac20b17c6ffbff24b0dc0d0f0c34b2";
let token = "4dbfed280e95515c00cef67a09afbdb0465f4a4966cf244ef62b5027d434873b";

export function getBoards() {
  return axios
    .get(`https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`)
    .then(resp => resp.data);
}

export function createBoard(name) {
  return axios
    .post(
      `https://api.trello.com/1/boards/?name=${name}&key=${key}&token=${token}`
    )
    .then(res => res.data);
}
export function getListsOfBoard(id) {
  return axios
    .get(
      `https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`
    )
    .then(resp => resp.data);
}
export function getCardsOfBoard(boardId) {
  return axios
    .get(
      `https://api.trello.com/1/boards/${boardId}/cards?key=${key}&token=${token}`
    )
    .then(resp => resp.data);
}
export function addNewList(id, list) {
  return axios
    .post(
      `https://api.trello.com/1/boards/${id}/lists?name=${list}&key=${key}&token=${token}`
    )
    .then(res => res.data);
}
export function deleteList(listId) {
  return axios
    .put(
      `https://api.trello.com/1/lists/${listId}/closed?key=${key}&token=${token}&value=true`
    )
    .then(res => res.data);
}
export function filterListsOfBoard(boardId) {
  return axios
    .get(
      `https://api.trello.com/1/boards/${boardId}/lists/open?key=${key}&token=${token}`
    )
    .then(res => res.data);
}

export function getCardsOfList(listId) {
  return axios
    .get(
      `https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`
    )
    .then(res => res.data);
}
export function addCardToList(listId, cardName) {
  return axios
    .post(
      `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${key}&token=${token}`
    )
    .then(res => res.data);
}
export function deleteCardFromList(cardId) {
  return axios.delete(
    `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`
  );
}
export function updateCards(boardId) {
  return axios
    .get(
      `https://api.trello.com/1/boards/${boardId}/cards/open?key=46ac20b17c6ffbff24b0dc0d0f0c34b2&token=4dbfed280e95515c00cef67a09afbdb0465f4a4966cf244ef62b5027d434873b`
    )
    .then(res => res.data);
}
