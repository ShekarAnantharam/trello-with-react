import React, { Component } from "react";

import * as API from "./API/api";
import "./styles.css";

import deleteIcon from "../Images/icon-delete.svg";
import closeIcon from "../Images/icon-close.svg";

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lists: [],
      cards: [],
      newList: "",
      newCardName: "",
      addList: "",
      addCard: "",
      showCardInputForList: "",
      deleteCard: ""
    };
  }
  //   displayAddCardDiv()
  handleDeleteAddListPopUp = () => {
    this.setState({ addList: false });
  };
  handleNewListName = event => {
    this.setState({ newList: event.target.value });
  };
  handleAddList = () => {
    this.setState({ addList: true });
  };
  handleAddedList = async () => {
    let listName = this.state.newList;
    let boardId = this.props.match.params.id;
    let newListadded = await API.addNewList(boardId, listName);

    this.setState({
      lists: [...this.state.lists, newListadded],
      newList: ""
    });
  };

  handleDeleteList = async listId => {
    let lists = this.state.lists.filter(list => list.id !== listId);
    this.setState({ lists });
    await API.deleteList(listId);
  };

  getLists = async boardId => {
    let data = await API.getListsOfBoard(boardId);

    this.setState({ lists: data });
  };
  getCardsOfBoard = async boardId => {
    let cards = await API.getCardsOfBoard(boardId);
    this.setState({ cards });
    console.log("cards", this.state.cards);
  };

  handleDeleteCard = async (listId, cardId) => {
    let cards = this.state.cards.filter(card => card.id !== cardId);
    this.setState({ cards });
    // this.setState({ deleteCard: cardId });
    // console.log("cardId", cardId);
    // console.log("listId",listId)
    // let boardId = this.props.match.params.id;
    await API.deleteCardFromList(cardId);
    //    let cards=this.state.cards.map(card=>{
    //        if(card.id==cardId){
    //            card.closed=true;
    //            return card
    //        }
    //        else{return card}
    //    });
    //    console.log("check",this.state.cards)
    //    console.log("check closed",cards)
    // this.setState({lists:lists})
    // const cards = await API.updateCards(boardId);
    //    console.log("check open",cardId)

    // this.setState({ cards: cards });
  };
  displayAddCardDiv = listId => {
    console.log("listiDFORCARD", listId);
    this.setState({
      addCard: true,
      showCardInputForList: listId
    });
  };
  s;
  handleCardInput = () => {
    this.setState({ addCard: false, showCardInputForList: "" });
  };
  handleAddCard = async listId => {
    let card = await API.addCardToList(listId, this.state.newCardName);
    console.log("newcard", card);
    this.setState({
      cards: [...this.state.cards, card],
      newCardName: ""
    });
  };
  handleNewCardName = event => {
    this.setState({ newCardName: event.target.value });
  };

  componentDidMount() {
    let boardId = this.props.match.params.id;
    console.log("boardId", boardId);
    this.getLists(boardId);
    this.getCardsOfBoard(boardId);
  }

  render() {
    console.log("routed", this.state.lists);

    return (
      <div className="container">
        <div className="lists ">
          <div>
            <div className="addList" onClick={this.handleAddList}>
              + add a new list
            </div>
            {this.state.addList ? (
              <div className="addListPopUp">
                <input
                  type="text"
                  className="form-control"
                  value={this.state.newList}
                  onChange={event => this.handleNewListName(event)}
                />
                <div className="list-popup-buttons">
                  <button
                    className="btn btn-primary button-add-list"
                    onClick={this.handleAddedList}
                  >
                    Add list
                  </button>
                  <button
                    className="btn btn-secondary button-add-list "
                    onClick={this.handleDeleteAddListPopUp}
                  >
                    {<img src={closeIcon} />}
                  </button>
                </div>
              </div>
            ) : null}
          </div>
          {this.state.lists.map(list => {
            return (
              <div className="list card bg-light" key={list.id}>
                {" "}
                <div className="list-content">
                  <p>
                    <strong>{list.name}</strong>
                  </p>
                  <button
                    className="delete-button"
                    onClick={() => {
                      this.handleDeleteList(list.id);
                    }}
                  >
                    {<img className="deleteIcon" src={deleteIcon} />}
                  </button>
                </div>
                {this.state.cards.map(card => {
                  if (list.id == card.idList) {
                    return (
                      <div className="card" key={card.id}>
                        <div className="card-in-list">
                          {card.name}
                          <button
                            onClick={() =>
                              this.handleDeleteCard(list.id, card.id)
                            }
                            className="delete-button"
                          >
                            {<img className="deleteIcon" src={deleteIcon} />}
                          </button>
                        </div>
                      </div>
                    );
                  }
                })}
                <div
                  key={list.id}
                  className="addCard"
                  onClick={() => this.displayAddCardDiv(list.id)}
                >
                  + Add a card
                </div>
                {this.state.addCard ? (
                  this.state.showCardInputForList == list.id ? (
                    <div className="show-add-card">
                      <input
                        type="text"
                        className="form-control card-input"
                        value={this.state.newCardName}
                        onChange={event => this.handleNewCardName(event)}
                      />
                      <div className="show-card-input">
                        <button
                          className="btn btn-primary"
                          onClick={() => this.handleAddCard(list.id)}
                        >
                          add card
                        </button>
                        <button
                          className="btn btn-secondary"
                          onClick={this.handleCardInput}
                        >
                          {<img src={closeIcon} />}
                        </button>
                      </div>
                    </div>
                  ) : null
                ) : null}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Board;
